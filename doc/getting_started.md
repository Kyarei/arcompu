## Getting Started with Arcane Computing

### Basic materials

#### Mossy Planks

```
     P = any planks
 PV  V = vines
     (shapeless)
```

#### Conductive Fabric

```
 R   R = redstone dust
RWR  W = any wool
 R 
```

#### Empty Casing

```
PPP  P = mossy planks
P P
PPP
```
