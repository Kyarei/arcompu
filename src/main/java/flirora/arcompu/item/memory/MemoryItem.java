package flirora.arcompu.item.memory;

import java.util.Arrays;

import flirora.arcompu.item.io.Peripheral;
import flirora.arcompu.item.io.PeripheralItem;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.world.World;

public class MemoryItem extends PeripheralItem {

  private final int size;
  private final boolean persistent;
  private final boolean writable;

  public MemoryItem(Settings settings, int size, boolean persistent,
      boolean writable) {
    super(settings.maxCount(1));
    this.size = size;
    this.persistent = persistent;
    this.writable = writable;
  }

  public int getSize() {
    return size;
  }

  public boolean isPersistent() {
    return persistent;
  }

  public boolean isWritable() {
    return writable;
  }

  public byte[] getMemory(ItemStack stack) {
    CompoundTag tag = stack.getOrCreateTag();
    if (tag.contains("MemoryContents", NbtType.BYTE_ARRAY)) {
      byte[] mem = tag.getByteArray("MemoryContents");
      if (mem.length == size)
        return mem;
      byte[] newMem = Arrays.copyOf(mem, size);
      tag.putByteArray("MemoryContents", newMem);
      return newMem;
    } else {
      byte[] mem = new byte[size];
      tag.putByteArray("MemoryContents", mem);
      return mem;
    }
  }

  public void setMemory(ItemStack stack, byte[] mem) {
    CompoundTag tag = stack.getOrCreateTag();
    tag.putByteArray("MemoryContents", mem);
  }

  public void clearMemory(ItemStack stack) {
    Arrays.fill(getMemory(stack), (byte) 0);
  }

  @Override
  public void inventoryTick(ItemStack itemStack, World world, Entity entity,
      int i, boolean bl) {
    // TODO Auto-generated method stub
    super.inventoryTick(itemStack, world, entity, i, bl);
  }

  public class AsPeripheral implements Peripheral {
    ItemStack stack;
    byte[] memory;

    AsPeripheral(ItemStack stack) {
      this.stack = stack;
      this.memory = getMemory(stack);
    }

    @Override
    public int getAddressSpace() {
      return getSize();
    }

    @Override
    public byte peek(int offset) {
      return memory[offset];
    }

    @Override
    public boolean poke(int offset, byte b) {
      if (!isWritable())
        return false;
      memory[offset] = b;
      return true;
    }

    @Override
    public Text name() {
      return MemoryItem.this.getName(stack);
    }
  }

  @Override
  public Peripheral toPeripheral(ItemStack stack) {
    return new AsPeripheral(stack);
  }

}
