package flirora.arcompu.item.memory;

import org.lwjgl.glfw.GLFW;

import flirora.arcompu.ArcaneComputingMod;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class RuneEditingWidget extends WWidget {
  private int nCols, nRows;
  private int x, y;
  private byte[] memory;
  private boolean readonly;

  public RuneEditingWidget(RunestoneItem item, ItemStack stack,
      boolean readonly) {
    nCols = item.getWidth();
    nRows = item.getHeight();
    memory = item.getMemory(stack);
    System.out.println(memory.length);
    this.readonly = readonly;
    this.setSize(nCols * 16, nRows * 16);
  }

  public byte[] getMemory() {
    return memory;
  }

  @Override
  public boolean canResize() {
    return false; // set to false if you want a static size
  }

  private static final Identifier SELECT =
      new Identifier(ArcaneComputingMod.MOD_ID, "textures/runes/select.png");

  @Environment(EnvType.CLIENT)
  @Override
  public void paintBackground(int x, int y, int mouseX, int mouseY) {
    super.paintBackground(x, y, mouseX, mouseY);
    for (int row = 0; row < nRows; ++row) {
      for (int col = 0; col < nCols; ++col) {
        int nibbleIndex = col + row * nCols;
        int b = memory[nibbleIndex / 2] & 255;
        int nibble = nibbleIndex % 2 == 0 ? (b >> 4) : (b & 15);
        ScreenDrawing.texturedRect(x + 16 * col, y + 16 * row, 16, 16,
            getTextureForNibble(nibble), 0xff_ffffff);
      }
    }
    if (!readonly)
      ScreenDrawing.texturedRect(x + 16 * this.x, y + 16 * this.y, 16, 16,
          SELECT, 0xff_ffffff);
  }

  private static Identifier getTextureForNibble(int nibble) {
    return new Identifier(ArcaneComputingMod.MOD_ID,
        String.format("textures/runes/%x.png", nibble));
  }

  @Override
  public boolean canFocus() {
    return true; // To make a widget able to become focused
  }

  @Override
  public void tick() {
    super.tick();
    requestFocus();
  }

  private int charToDigit(char ch) {
    if (ch >= '0' && ch <= '9')
      return ch - '0';
    if (ch >= 'a' && ch <= 'f')
      return ch - 'a' + 10;
    return -1;
  }

  @Override
  public void onCharTyped(char ch) {
    if (readonly)
      return;
    int digit = charToDigit(ch);
    if (digit == -1)
      return;
    int nibbleIndex = x + y * nCols;
    if (nibbleIndex % 2 == 0) {
      memory[nibbleIndex / 2] =
          (byte) ((memory[nibbleIndex / 2] & 15) | (digit << 4));
    } else {
      memory[nibbleIndex / 2] =
          (byte) ((memory[nibbleIndex / 2] & ~15) | digit);
    }
    ++x;
    if (x >= nCols) {
      x -= nCols;
      ++y;
      if (y >= nRows)
        y -= nRows;
    }
  }

  @Override
  public void onKeyPressed(int ch, int key, int modifiers) {
    if (readonly)
      return;
    switch (ch) {
    case GLFW.GLFW_KEY_LEFT:
      --x;
      break;
    case GLFW.GLFW_KEY_RIGHT:
      ++x;
      break;
    case GLFW.GLFW_KEY_UP:
      --y;
      break;
    case GLFW.GLFW_KEY_DOWN:
      ++y;
      break;
    default:
      return;
    }
    if (x < 0)
      x += nCols;
    if (x >= nCols)
      x -= nCols;
    if (y < 0)
      y += nRows;
    if (y >= nRows)
      y -= nRows;
  }

}
