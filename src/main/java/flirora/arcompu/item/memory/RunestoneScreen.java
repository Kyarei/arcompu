package flirora.arcompu.item.memory;

import flirora.arcompu.ArcaneComputingMod;
import io.github.cottonmc.cotton.gui.client.CottonClientScreen;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.util.Hand;
import net.minecraft.util.PacketByteBuf;

@Environment(EnvType.CLIENT)
public class RunestoneScreen extends CottonClientScreen {
  private RunestoneGui runestoneGui;
  private Hand hand;

  public RunestoneScreen(RunestoneGui description, Hand hand) {
    super(description);
    runestoneGui = description;
    this.hand = hand;
  }

  @Override
  public void onClose() {
    super.onClose();
    byte[] newMem = runestoneGui.getEditor().getMemory();
    PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
    data.writeBoolean(hand == Hand.OFF_HAND);
    data.writeByteArray(newMem);
    ClientSidePacketRegistry.INSTANCE
        .sendToServer(ArcaneComputingMod.EDIT_RUNESTONE_PACKET_ID, data);
  }

}
