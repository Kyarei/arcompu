package flirora.arcompu.item.memory;

import flirora.arcompu.item.ChiselItem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

/**
 * A ROM item with an editor GUI.
 * 
 * A runestone is edited by nibbles.
 * 
 */
public class RunestoneItem extends MemoryItem {

  private final int width;
  private final int height;

  public RunestoneItem(Settings settings, int width, int height) {
    super(settings, (width * height + 1) / 2, true, false);
    this.width = width;
    this.height = height;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  @Environment(EnvType.CLIENT)
  @Override
  public TypedActionResult<ItemStack> use(World world,
      PlayerEntity playerEntity, Hand hand) {
    if (world.isClient) {
      Hand oppositeHand =
          hand == Hand.MAIN_HAND ? Hand.OFF_HAND : Hand.MAIN_HAND;
      ItemStack otherHandStack = playerEntity.getStackInHand(oppositeHand);
      boolean readonly = !(otherHandStack.getItem() instanceof ChiselItem);
      MinecraftClient.getInstance().openScreen(new RunestoneScreen(
          new RunestoneGui(this, playerEntity.getStackInHand(hand), readonly),
          hand));
      return TypedActionResult.success(playerEntity.getStackInHand(hand));
    }
    return super.use(world, playerEntity, hand);
  }

  public static void updateData(PlayerEntity player, boolean isOffHand,
      byte[] newData) {
    ItemStack stack =
        player.getStackInHand(isOffHand ? Hand.OFF_HAND : Hand.MAIN_HAND);
    if (!(stack.getItem() instanceof RunestoneItem))
      return;
    RunestoneItem item = (RunestoneItem) stack.getItem();
    ItemStack offHandStack =
        player.getStackInHand(isOffHand ? Hand.MAIN_HAND : Hand.OFF_HAND);
    if (!(offHandStack.getItem() instanceof ChiselItem))
      return;
    byte[] oldData = item.getMemory(stack);
    if (newData.length != oldData.length)
      return;
    int differentNibbles = 0;
    for (int i = 0; i < newData.length; ++i) {
      byte a = oldData[i], b = newData[i];
      if ((a >>> 4) != (b >>> 4))
        ++differentNibbles;
      if ((a & 15) != (b & 15))
        ++differentNibbles;
    }
    offHandStack.damage(differentNibbles, player.getRandom(), null);
    item.setMemory(stack, newData);
  }

}
