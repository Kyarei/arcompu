package flirora.arcompu.item.memory;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.ItemStack;

@Environment(EnvType.CLIENT)
public class RunestoneGui extends LightweightGuiDescription {
  private RuneEditingWidget editor;

  public RunestoneGui(RunestoneItem item, ItemStack stack, boolean readonly) {
    WGridPanel root = new WGridPanel();
    setRootPanel(root);

    editor = new RuneEditingWidget(item, stack, readonly);
    root.add(editor, 0, 0);

    root.validate(this);
  }

  public RuneEditingWidget getEditor() {
    return editor;
  }

  @Override
  public void addPainters() {
    getRootPanel()
        .setBackgroundPainter(BackgroundPainter.createColorful(0xff_282d36));
  }

}
