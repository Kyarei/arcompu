package flirora.arcompu.item.io;

import net.minecraft.text.Text;

public interface Peripheral {
  int getAddressSpace();

  byte peek(int offset);

  boolean poke(int offset, byte b);

  Text name();
}
