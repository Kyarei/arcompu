package flirora.arcompu.item.io;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public abstract class PeripheralItem extends Item {

  public PeripheralItem(Settings settings) {
    super(settings);
  }

  public abstract Peripheral toPeripheral(ItemStack stack);

}
