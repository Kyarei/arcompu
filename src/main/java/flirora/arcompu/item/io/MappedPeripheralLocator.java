package flirora.arcompu.item.io;

import com.google.common.collect.Range;

import datastructures.Interval;

public class MappedPeripheralLocator implements Interval {

  private final PeripheralLocator locator;
  private final int start, end;

  public MappedPeripheralLocator(PeripheralLocator locator, int start,
      int end) {
    super();
    this.locator = locator;
    this.start = start;
    this.end = end;
  }

  public MappedPeripheralLocator(PeripheralLocator loc, Range<Integer> range) {
    this(loc, range.lowerEndpoint(), range.upperEndpoint());
  }

  @Override
  public int start() {
    return start;
  }

  @Override
  public int end() {
    return end;
  }

  public PeripheralLocator getLocator() {
    return locator;
  }

  public int size() {
    return end - start;
  }

  public MappedPeripheralLocator shift(int offset) {
    return new MappedPeripheralLocator(locator, start + offset, end + offset);
  }

  public MappedPeripheralLocator rebase(int newStart) {
    return new MappedPeripheralLocator(locator, newStart,
        end - start + newStart);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + end;
    result = prime * result + ((locator == null) ? 0 : locator.hashCode());
    result = prime * result + start;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    MappedPeripheralLocator other = (MappedPeripheralLocator) obj;
    if (end != other.end)
      return false;
    if (locator == null) {
      if (other.locator != null)
        return false;
    } else if (!locator.equals(other.locator))
      return false;
    if (start != other.start)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "MappedPeripheralLocator [locator=" + locator + ", start=" + start
        + ", end=" + end + "]";
  }

}
