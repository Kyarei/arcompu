package flirora.arcompu.item.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import com.mojang.datafixers.util.Pair;

import blue.endless.jankson.annotation.Nullable;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;

public class AddressMap {
  /*
   * Invariants:
   * 
   * 
   * 1. for every (k, v) in mapsByLocator, v is in addressMap
   * 
   * 2. for every v in addressMap, there is a (k, v) in mapsByLocator
   * 
   * 3. for every v in addressMap, v.getLocator() is unique
   * 
   * 4. for every (k, v) in mapsByLocator, k.equals(v.getLocator())
   */
  private Map<PeripheralLocator, Optional<Range<Integer>>> reverseMappings;
  private RangeMap<Integer, PeripheralLocator> mappings;

  public AddressMap() {
    mappings = TreeRangeMap.create();
    reverseMappings = new HashMap<>();
  }

  public boolean add(MappedPeripheralLocator map) {
    Range<Integer> interval = Range.closedOpen(map.start(), map.end());
    if (!mappings.subRangeMap(interval).asMapOfRanges().isEmpty()) {
      reverseMappings.put(map.getLocator(), Optional.empty());
      return false;
    }
    mappings.put(interval, map.getLocator());
    reverseMappings.put(map.getLocator(), Optional.of(interval));
    return true;
  }

  public boolean remove(PeripheralLocator loc) {
    Optional<Range<Integer>> interval = reverseMappings.get(loc);
    if (interval == null)
      return false;
    Optional<Range<Integer>> removedRange = reverseMappings.remove(loc);
    assert removedRange != null;
    if (interval.isPresent())
      mappings.remove(interval.get());
    return true;
  }

  public void removeIf(Predicate<MappedPeripheralLocator> pred) {
    List<MappedPeripheralLocator> pendingRemovals = new ArrayList<>();
    mappings().forEach((elem) -> {
      if (pred.test(elem))
        pendingRemovals.add(elem);
    });
    for (MappedPeripheralLocator removed : pendingRemovals) {
      remove(removed.getLocator());
    }
  }

  public boolean tryRebase(PeripheralLocator locator, int newAddress, int size,
      int addressSpaceSize) {
    if (newAddress + size > addressSpaceSize)
      return false;
    Optional<Range<Integer>> orange = reverseMappings.get(locator);
    if (orange == null)
      return false;
    remove(locator);
    return add(
        new MappedPeripheralLocator(locator, newAddress, newAddress + size));
  }

  public boolean unbase(PeripheralLocator locator) {
    if (!remove(locator))
      return false;
    reverseMappings.put(locator, Optional.empty());
    return true;
  }

  public Stream<MappedPeripheralLocator> mappings() {
    return mappings.asMapOfRanges().entrySet().stream()
        .map((e) -> new MappedPeripheralLocator(e.getValue(),
            e.getKey().lowerEndpoint(), e.getKey().upperEndpoint()));
  }

  public Stream<Entry<PeripheralLocator, Optional<Range<Integer>>>> peripheralsAndMappings() {
    return reverseMappings.entrySet().stream();
  }

  @Nullable
  public Pair<PeripheralLocator, Integer> dereference(int address) {
    Entry<Range<Integer>, PeripheralLocator> entry = mappings.getEntry(address);
    if (entry == null)
      return null;
    return Pair.of(entry.getValue(), address - entry.getKey().lowerEndpoint());
  }

  public int findFreeOffset(int size, int totalSpace) {
    int lastEnding = 0;
    for (Range<Integer> interval : mappings.asMapOfRanges().keySet()) {
      if (interval.lowerEndpoint() - lastEnding >= size)
        return lastEnding;
      lastEnding = interval.upperEndpoint();
    }
    if (totalSpace - lastEnding >= size)
      return lastEnding;
    return -1;
  }

  public void readFromTag(CompoundTag tag) {
    reverseMappings.clear();
    mappings.clear();
    ListTag addrs = tag.getList("AddressMap", NbtType.COMPOUND);
    for (int i = 0; i < addrs.size(); ++i) {
      CompoundTag elem = addrs.getCompound(i);
      if (elem.contains("start") && elem.contains("end")) {
        int start = elem.getInt("start");
        int end = elem.getInt("end");
        PeripheralLocator locator = PeripheralLocator.fromTag(elem);
        this.add(new MappedPeripheralLocator(locator, start, end));
      } else {
        reverseMappings.put(PeripheralLocator.fromTag(elem), Optional.empty());
      }
    }
  }

  public static AddressMap fromTag(CompoundTag tag) {
    AddressMap res = new AddressMap();
    res.readFromTag(tag);
    return res;
  }

  public void writeToTag(CompoundTag tag) {
    ListTag addrs = new ListTag();
    for (Entry<PeripheralLocator, Optional<Range<Integer>>> entry : reverseMappings
        .entrySet()) {
      PeripheralLocator locator = entry.getKey();
      Optional<Range<Integer>> orange = entry.getValue();
      CompoundTag elem = locator.toTag();
      if (orange.isPresent()) {
        elem.putInt("start", orange.get().lowerEndpoint());
        elem.putInt("end", orange.get().upperEndpoint());
      }
      addrs.add(elem);
    }
    tag.put("AddressMap", addrs);
  }

  @Override
  public String toString() {
    return "AddressMap [reverseMappings=" + reverseMappings + ", mappings="
        + mappings + "]";
  }
}
