package flirora.arcompu.item.io;

import java.io.Serializable;

import blue.endless.jankson.annotation.Nullable;
import flirora.arcompu.block.SystemCoreBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.PacketByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public final class PeripheralLocator
    implements Comparable<PeripheralLocator>, Serializable {
  private static final long serialVersionUID = 4780107968358691860L;
  private final PeripheralLocator.Entry entry;

  private PeripheralLocator(Entry entry) {
    this.entry = entry;
  }

  public static PeripheralLocator fromItemIndex(int id) {
    return new PeripheralLocator(new ItemEntry(id));
  }

  public static PeripheralLocator fromBlockOffset(Vec3i offset) {
    return new PeripheralLocator(new BlockEntry(offset));
  }

  public Peripheral getPeripheral(World world, SystemCoreBlockEntity system) {
    return entry.getPeripheral(world, system);
  }

  public CompoundTag toTag() {
    return entry.toTag(new CompoundTag());
  }

  public static PeripheralLocator fromTag(CompoundTag elem) {
    if (elem.contains("slot")) {
      return fromItemIndex(elem.getInt("slot"));
    } else {
      return fromBlockOffset(
          new Vec3i(elem.getInt("x"), elem.getInt("y"), elem.getInt("z")));
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((entry == null) ? 0 : entry.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PeripheralLocator other = (PeripheralLocator) obj;
    if (entry == null) {
      if (other.entry != null)
        return false;
    } else if (!entry.equals(other.entry))
      return false;
    return true;
  }

  @Override
  public int compareTo(PeripheralLocator o) {
    if (entry instanceof ItemEntry) {
      if (o.entry instanceof ItemEntry) {
        return ((ItemEntry) entry).compareTo((ItemEntry) o.entry);
      } else {
        return -1;
      }
    } else if (o.entry instanceof ItemEntry) {
      return 1;
    } else {
      return ((BlockEntry) entry).compareTo((BlockEntry) o.entry);
    }
  }

  public int getPeripheralId() {
    if (entry instanceof ItemEntry)
      return ((ItemEntry) entry).getPeripheralId();
    return -1;
  }

  @Nullable
  public Vec3i getBlockOffset() {
    if (entry instanceof BlockEntry)
      return ((BlockEntry) entry).getBlockOffset();
    return null;
  }

  public static PeripheralLocator read(PacketByteBuf buf) {
    boolean isBlock = buf.readBoolean();
    if (isBlock) {
      return fromBlockOffset(
          new Vec3i(buf.readInt(), buf.readInt(), buf.readInt()));
    } else {
      return fromItemIndex(buf.readInt());
    }
  }

  public void write(PacketByteBuf buf) {
    entry.write(buf);
  }

  @Override
  public String toString() {
    return entry.toString();
  }

  private interface Entry {
    @Nullable
    Peripheral getPeripheral(World world, SystemCoreBlockEntity system);

    CompoundTag toTag(CompoundTag tag);

    void write(PacketByteBuf buf);
  }

  private static class ItemEntry implements Entry, Comparable<ItemEntry> {
    private final int peripheralId;

    public ItemEntry(int peripheralId) {
      this.peripheralId = peripheralId;
    }

    @Override
    public Peripheral getPeripheral(World world, SystemCoreBlockEntity system) {
      return system.getPeripheral(peripheralId);
    }

    public int getPeripheralId() {
      return peripheralId;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + peripheralId;
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      ItemEntry other = (ItemEntry) obj;
      if (peripheralId != other.peripheralId)
        return false;
      return true;
    }

    @Override
    public int compareTo(ItemEntry other) {
      return Integer.compare(peripheralId, other.peripheralId);
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
      tag.putInt("slot", peripheralId);
      return tag;
    }

    @Override
    public void write(PacketByteBuf buf) {
      buf.writeBoolean(false);
      buf.writeInt(peripheralId);
    }

    @Override
    public String toString() {
      return "Peripheral Item #" + peripheralId;
    }
  }

  private static class BlockEntry implements Entry, Comparable<BlockEntry> {
    private final Vec3i blockOffset;

    public BlockEntry(Vec3i blockOffset) {
      this.blockOffset = blockOffset;
    }

    @Override
    public Peripheral getPeripheral(World world, SystemCoreBlockEntity system) {
      BlockPos position = system.getPos().add(blockOffset);
      BlockEntity be = world.getBlockEntity(position);
      if (be == null)
        return null;
      return be instanceof Peripheral ? (Peripheral) be : null;
    }
    //

    public Vec3i getBlockOffset() {
      return blockOffset;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result =
          prime * result + ((blockOffset == null) ? 0 : blockOffset.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      BlockEntry other = (BlockEntry) obj;
      if (blockOffset == null) {
        if (other.blockOffset != null)
          return false;
      } else if (!blockOffset.equals(other.blockOffset))
        return false;
      return true;
    }

    @Override
    public int compareTo(BlockEntry other) {
      return blockOffset.compareTo(other.blockOffset);
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
      tag.putInt("x", blockOffset.getX());
      tag.putInt("y", blockOffset.getY());
      tag.putInt("z", blockOffset.getZ());
      return tag;
    }

    @Override
    public void write(PacketByteBuf buf) {
      buf.writeBoolean(true);
      buf.writeInt(blockOffset.getX());
      buf.writeInt(blockOffset.getY());
      buf.writeInt(blockOffset.getZ());
    }

    @Override
    public String toString() {
      return "Peripheral Block @+ " + blockOffset;
    }
  }
}
