package flirora.arcompu.item;

import net.minecraft.item.Item;

public class ChiselItem extends Item {

  public ChiselItem(Settings settings) {
    super(settings.maxCount(1).maxDamage(260));
  }

}
