package flirora.arcompu.item.cpu;

import flirora.arcompu.block.SystemCoreBlockEntity;
import net.minecraft.nbt.CompoundTag;

public interface CpuState<C extends CpuItem> {
  /**
   * Execute the CPU for `clockCycles` cycles.
   * 
   * @param system      the system core block entity this CPU belongs to
   * @param clockCycles the number of clock cycles allotted
   * 
   * @return the number of clock cycles used
   */
  int advance(SystemCoreBlockEntity system, int clockCycles);

  CompoundTag toTag(CompoundTag tag);

  void fromTag(CompoundTag tag);
}
