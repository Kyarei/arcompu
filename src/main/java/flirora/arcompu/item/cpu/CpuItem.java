package flirora.arcompu.item.cpu;

import net.minecraft.item.Item;

public abstract class CpuItem extends Item {

  private final int addressSpaceSize;
  private final int clockSpeed; // in cycles per tick

  public CpuItem(Settings settings, int addressSpaceSize, int clockSpeed) {
    super(settings);
    this.addressSpaceSize = addressSpaceSize;
    this.clockSpeed = clockSpeed;
  }

  public int getAddressSpaceSize() {
    return addressSpaceSize;
  }

  public int getClockSpeed() {
    return clockSpeed;
  }

  public abstract CpuState<?> getBootState();

}
