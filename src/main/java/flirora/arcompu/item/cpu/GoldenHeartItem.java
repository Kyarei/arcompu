package flirora.arcompu.item.cpu;

import flirora.arcompu.block.SystemCoreBlockEntity;
import net.minecraft.nbt.CompoundTag;

/**
 * A 4-bit processor.
 * 
 * It has the following registers:
 * 
 * - a, the 4-bit accumulator
 * 
 * - b, c, d and e, 4 bits each, which come in the 8-byte pairs bc and de
 * 
 * - the flags register, with z (zero?) and r (carry?)
 * 
 * - ip, the 8-bit instruction pointer, pointing to a single nibble
 *
 */
public class GoldenHeartItem extends CpuItem {

  public GoldenHeartItem(Settings settings) {
    super(settings, 128, 20);
  }

  @Override
  public CpuState<?> getBootState() {
    return new State();
  }

  public static class State implements CpuState<GoldenHeartItem> {

    byte a, bc, de;
    boolean zero, carry;
    byte ip;

    public State() {
      a = 0;
      bc = 0;
      de = 0;
      zero = false;
      carry = false;
      ip = 0;
    }

    private byte getNibble(SystemCoreBlockEntity system, int address) {
      address &= 255;
      byte b = system.peek(address / 2);
      if (b % 2 == 0)
        return (byte) (b & 15);
      return (byte) (b >>> 4);
    }

    private boolean putNibble(SystemCoreBlockEntity system, int address,
        byte nibble) {
      address &= 255;
      byte b = system.peek(address / 2);
      if (b % 2 == 0)
        return system.poke(address / 2, (byte) ((b & ~15) | nibble));
      return system.poke(address / 2, (byte) ((b & 15) | (nibble << 4)));
    }

    @Override
    public int advance(SystemCoreBlockEntity system, int clockCycles) {
      int cyclesUsed = 0;
      while (cyclesUsed < clockCycles) {
        byte instruction = getNibble(system, ip);
        byte x = 0, y = 0;
        switch (instruction) {
        case 0: // ld a, imm4
          a = getNibble(system, ip + 1);
          ip += 2;
          cyclesUsed += 3;
          break;
        case 1: // ld a, x
          x = getNibble(system, ip + 1);
          switch (x % 4) {
          case 0:
            a = (byte) (bc >>> 4);
            break;
          case 1:
            a = (byte) (bc & 15);
            break;
          case 2:
            a = (byte) (de >>> 4);
            break;
          case 3:
            a = (byte) (de & 15);
            break;
          }
          ip += 2;
          cyclesUsed += 3;
          break;
        case 2: // ld x, a
          x = getNibble(system, ip + 1);
          switch (x % 4) {
          case 0:
            bc = (byte) ((bc & 15) & (a << 4));
            break;
          case 1:
            bc = (byte) ((bc & ~15) & a);
            break;
          case 2:
            de = (byte) ((de & 15) & (a << 4));
            break;
          case 3:
            de = (byte) ((de & ~15) & a);
            break;
          }
          ip += 2;
          cyclesUsed += 3;
          break;
        case 3: // add b
          a += (byte) (bc >>> 4);
          updateFlags();
          ++ip;
          cyclesUsed += 3;
          break;
        case 4: // sub b
          a -= (byte) (bc >>> 4);
          updateFlags();
          ++ip;
          cyclesUsed += 3;
          break;
        case 5: // shr
          a >>= 1;
          updateFlags();
          ++ip;
          cyclesUsed += 2;
          break;
        case 6: // and c
          a &= (byte) (bc & 15);
          updateFlags();
          ++ip;
          cyclesUsed += 3;
          break;
        case 7: // or c
          a |= (byte) (bc & 15);
          updateFlags();
          ++ip;
          cyclesUsed += 3;
          break;
        case 8: // not
          a = (byte) ~a;
          updateFlags();
          carry = false;
          ++ip;
          cyclesUsed += 2;
          break;
        case 9: // ld a, [bc]
          a = getNibble(system, bc);
          ++ip;
          cyclesUsed += 3;
          break;
        case 10: // ld a, [de]
          a = getNibble(system, de);
          ++ip;
          cyclesUsed += 3;
          break;
        case 11: // ld [bc], a
          putNibble(system, bc, a);
          ++ip;
          cyclesUsed += 3;
          break;
        case 12: // ld [de], a
          putNibble(system, de, a);
          ++ip;
          cyclesUsed += 3;
          break;
        case 13: // ld a, [imm8]
          x = getNibble(system, ip + 1);
          y = getNibble(system, ip + 2);
          a = getNibble(system, x + (y << 4));
          ip += 3;
          cyclesUsed += 5;
          break;
        case 14: // jz [imm4]
          cyclesUsed += 4;
          if (zero) {
            x = getNibble(system, ip + 1);
            ip += getOffset(x);
          } else {
            ip += 2;
          }
          break;
        case 15: // jc [imm4]
          cyclesUsed += 4;
          if (carry) {
            x = getNibble(system, ip + 1);
            ip += getOffset(x);
          } else {
            ip += 2;
          }
          break;
        }
      }
      return cyclesUsed;
    }

    private byte getOffset(byte imm4) {
      byte signmask = (byte) (imm4 & 8);
      return (byte) (imm4 | (signmask * 30));
    }

    private void updateFlags() {
      carry = a >= 16 || a < 0;
      a %= 16;
      zero = a == 0;
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
      tag.putByte("a", a);
      tag.putByte("bc", bc);
      tag.putByte("de", de);
      tag.putByte("ip", ip);
      tag.putBoolean("zero", zero);
      tag.putBoolean("carry", carry);
      return tag;
    }

    @Override
    public void fromTag(CompoundTag tag) {
      a = tag.getByte("a");
      bc = tag.getByte("bc");
      de = tag.getByte("de");
      ip = tag.getByte("ip");
      zero = tag.getBoolean("zero");
      carry = tag.getBoolean("carry");
    }

  }

}
