package flirora.arcompu;

import flirora.arcompu.block.AssemblyTableBlock;
import flirora.arcompu.block.SystemCoreBlock;
import flirora.arcompu.block.gui.AssemblyTableController;
import flirora.arcompu.block.gui.AssemblyTableScreen;
import flirora.arcompu.block.gui.SystemCoreController;
import flirora.arcompu.block.gui.SystemCoreScreen;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.screen.ScreenProviderRegistry;
import net.minecraft.container.BlockContext;

@Environment(EnvType.CLIENT)
public class ArcaneComputingModClient implements ClientModInitializer {

  @Override
  public void onInitializeClient() {
    ScreenProviderRegistry.INSTANCE.registerFactory(AssemblyTableBlock.ID,
        (syncId, identifier, player, buf) -> new AssemblyTableScreen(
            new AssemblyTableController(syncId, player.inventory,
                BlockContext.create(player.world, buf.readBlockPos())),
            player));
    ScreenProviderRegistry.INSTANCE.registerFactory(SystemCoreBlock.ID,
        (syncId, identifier, player, buf) -> new SystemCoreScreen(
            new SystemCoreController(syncId, player.inventory,
                BlockContext.create(player.world, buf.readBlockPos())),
            player));
  }

}
