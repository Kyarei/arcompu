package flirora.arcompu;

import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableSet;

import flirora.arcompu.block.AssemblyTableBlock;
import flirora.arcompu.block.AssemblyTableBlockEntity;
import flirora.arcompu.block.CasingBlock;
import flirora.arcompu.block.SystemCoreBlock;
import flirora.arcompu.block.SystemCoreBlockEntity;
import flirora.arcompu.block.gui.AssemblyTableController;
import flirora.arcompu.block.gui.SystemCoreController;
import flirora.arcompu.item.ChiselItem;
import flirora.arcompu.item.cpu.GoldenHeartItem;
import flirora.arcompu.item.io.PeripheralLocator;
import flirora.arcompu.item.memory.MemoryItem;
import flirora.arcompu.item.memory.RunestoneItem;
import flirora.arcompu.recipe.AssemblyTableRecipe;
import flirora.arcompu.recipe.AssemblyTableRecipeSerializer;
import flirora.arcompu.recipe.VakTypes;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.container.BlockContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.loot.ConstantLootTableRange;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class ArcaneComputingMod implements ModInitializer {

  public static Logger LOGGER = LogManager.getLogger();

  public static final String MOD_ID = "arcompu";
  public static final String MOD_NAME = "Arcane Computing";

  public static final Block CONDUCTIVE_FABRIC =
      new Block(FabricBlockSettings.of(Material.WOOL).resistance(0.8f)
          .hardness(0.8f).sounds(BlockSoundGroup.WOOL).build());
  public static final Block MOSSY_PLANKS =
      new Block(FabricBlockSettings.of(Material.WOOD).strength(2.0F, 3.0F)
          .sounds(BlockSoundGroup.WOOD).build());
  public static final Block EMPTY_CASING =
      new CasingBlock(FabricBlockSettings.of(Material.WOOD).strength(2.0F, 3.0F)
          .sounds(BlockSoundGroup.WOOD).build());
  public static final Block ASSEMBLY_TABLE =
      new AssemblyTableBlock(FabricBlockSettings.of(Material.WOOD)
          .strength(2.0F, 3.0F).sounds(BlockSoundGroup.WOOD).build());
  public static final Block SYSTEM_CORE =
      new SystemCoreBlock(FabricBlockSettings.of(Material.WOOD)
          .strength(2.0F, 3.0F).sounds(BlockSoundGroup.WOOD).build());
  public static final Block ENCHANTED_STONE =
      new Block(FabricBlockSettings.of(Material.STONE).strength(2.5F, 9.0F)
          .sounds(BlockSoundGroup.STONE).build());

  public static final Item STRANGE_HEART =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item GHOSTLY_PAGE =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item CHISEL =
      new ChiselItem(new Item.Settings().group(ItemGroup.TOOLS));
  public static final Item LEP_RUNESTONE =
      new RunestoneItem(new Item.Settings().group(ItemGroup.REDSTONE), 8, 8);
  public static final Item LEP_PAGE = new MemoryItem(
      new Item.Settings().group(ItemGroup.REDSTONE), 8, false, true);
  public static final Item GOLDEN_HEART =
      new GoldenHeartItem(new Item.Settings().group(ItemGroup.REDSTONE));

  public static BlockEntityType<SystemCoreBlockEntity> SYSTEM_CORE_BLOCK_ENTITY;
  public static BlockEntityType<AssemblyTableBlockEntity> ASSEMBLY_TABLE_BLOCK_ENTITY;

  public static final Identifier EDIT_RUNESTONE_PACKET_ID =
      new Identifier(MOD_ID, "edit_runestone");
  public static final Identifier REQUEST_REBASE_PACKET_ID =
      new Identifier(MOD_ID, "request_rebase");

  private static void registerBlock(Identifier id, Block block,
      Item.Settings settings) {
    Registry.register(Registry.BLOCK, id, block);
    Registry.register(Registry.ITEM, id, new BlockItem(block, settings));
  }

  @Override
  public void onInitialize() {
    log(Level.INFO, "Initializing");

    // Blocks

    registerBlock(new Identifier(MOD_ID, "conductive_fabric"),
        CONDUCTIVE_FABRIC, new Item.Settings().group(ItemGroup.REDSTONE));
    registerBlock(new Identifier(MOD_ID, "mossy_planks"), MOSSY_PLANKS,
        new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    registerBlock(new Identifier(MOD_ID, "empty_casing"), EMPTY_CASING,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    registerBlock(AssemblyTableBlock.ID, ASSEMBLY_TABLE,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    registerBlock(SystemCoreBlock.ID, SYSTEM_CORE,
        new Item.Settings().group(ItemGroup.REDSTONE));
    registerBlock(new Identifier(MOD_ID, "enchanted_stone"), ENCHANTED_STONE,
        new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));

    // Items
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "strange_heart"),
        STRANGE_HEART);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "ghostly_page"),
        GHOSTLY_PAGE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "chisel"), CHISEL);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "lep_runestone"),
        LEP_RUNESTONE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "lep_page"),
        LEP_PAGE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "golden_heart"),
        GOLDEN_HEART);

    // Block entities

    SYSTEM_CORE_BLOCK_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE,
        new Identifier(MOD_ID, "system_core"), BlockEntityType.Builder
            .create(SystemCoreBlockEntity::new, SYSTEM_CORE).build(null));
    ASSEMBLY_TABLE_BLOCK_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE,
        new Identifier(MOD_ID, "assembly_table"), BlockEntityType.Builder
            .create(AssemblyTableBlockEntity::new, ASSEMBLY_TABLE).build(null));

    // Recipe types

    Registry.register(Registry.RECIPE_SERIALIZER,
        AssemblyTableRecipeSerializer.ID,
        AssemblyTableRecipeSerializer.INSTANCE);
    Registry.register(Registry.RECIPE_TYPE,
        new Identifier(MOD_ID, AssemblyTableRecipe.Type.ID),
        AssemblyTableRecipe.Type.INSTANCE);

    // GUIs

    ContainerProviderRegistry.INSTANCE.registerFactory(AssemblyTableBlock.ID,
        (syncId, id, player, buf) -> new AssemblyTableController(syncId,
            player.inventory,
            BlockContext.create(player.world, buf.readBlockPos())));
    ContainerProviderRegistry.INSTANCE.registerFactory(SystemCoreBlock.ID,
        (syncId, id, player, buf) -> new SystemCoreController(syncId,
            player.inventory,
            BlockContext.create(player.world, buf.readBlockPos())));

    // Loot tables
    tweakLootTables();

    // Vak types
    VakTypes.registerAll();

    // Packets
    ServerSidePacketRegistry.INSTANCE.register(EDIT_RUNESTONE_PACKET_ID,
        (packetContext, attachedData) -> {
          boolean isOffHand = attachedData.readBoolean();
          byte[] newData = attachedData.readByteArray();
          packetContext.getTaskQueue().execute(() -> RunestoneItem
              .updateData(packetContext.getPlayer(), isOffHand, newData));
        });
    ServerSidePacketRegistry.INSTANCE.register(REQUEST_REBASE_PACKET_ID,
        (packetContext, attachedData) -> {
          BlockPos pos = attachedData.readBlockPos();
          PeripheralLocator locator = PeripheralLocator.read(attachedData);
          int address = attachedData.readInt();
          packetContext.getTaskQueue().execute(() -> {
            PlayerEntity player = packetContext.getPlayer();
            World world = player.world;
            if (player.getPos().squaredDistanceTo(pos.getX() + 0.5,
                pos.getY() + 0.5, pos.getZ() + 0.5) > 16 * 16)
              return;
            BlockEntity be = world.getBlockEntity(pos);
            if (!(be instanceof SystemCoreBlockEntity))
              return;
            ((SystemCoreBlockEntity) be).tryRebase(locator, address);
          });
        });
  }

  public static void log(Level level, String message) {
    LOGGER.log(level, "[" + MOD_NAME + "] " + message);
  }

  private static final Set<Identifier> LOOT_TABLES_WITH_STRANGE_HEART =
      ImmutableSet.of(new Identifier("minecraft", "entities/zombie"),
          new Identifier("minecraft", "entities/husk"),
          new Identifier("minecraft", "entities/zombie_villager"),
          new Identifier("minecraft", "entities/drowned"),
          new Identifier("minecraft", "entities/zombified_piglin"),
          new Identifier("minecraft", "entities/enderman"));

  private static void tweakLootTables() {
    LootTableLoadingCallback.EVENT
        .register((resourceManager, lootManager, id, supplier, setter) -> {
          if (LOOT_TABLES_WITH_STRANGE_HEART.contains(id)) {
            FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder.builder()
                .withCondition(RandomChanceLootCondition.builder(0.01f).build())
                .withRolls(ConstantLootTableRange.create(1))
                .withEntry(ItemEntry.builder(STRANGE_HEART));

            supplier.withPool(poolBuilder);
          }
        });
  }

}
