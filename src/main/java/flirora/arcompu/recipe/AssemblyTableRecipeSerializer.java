package flirora.arcompu.recipe;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import flirora.arcompu.ArcaneComputingMod;
import flirora.arcompu.ArcaneComputingRegistries;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.util.DefaultedList;
import net.minecraft.util.Identifier;
import net.minecraft.util.PacketByteBuf;
import net.minecraft.util.registry.Registry;

public class AssemblyTableRecipeSerializer
    implements RecipeSerializer<AssemblyTableRecipe> {

  public static final AssemblyTableRecipeSerializer INSTANCE =
      new AssemblyTableRecipeSerializer();

  public static final Identifier ID =
      new Identifier(ArcaneComputingMod.MOD_ID, "assembly_table_recipe");

  private AssemblyTableRecipeSerializer() {
  }

  private DefaultedList<Ingredient> getIngredients(JsonArray pattern,
      JsonObject key) {
    DefaultedList<Ingredient> ingredients =
        DefaultedList.ofSize(9, Ingredient.EMPTY);
    if (pattern.size() != 3) {
      throw new JsonParseException("Recipe must be 3x3");
    }
    for (int i = 0; i < 3; ++i) {
      String row = pattern.get(i).getAsString();
      if (row.length() != 3) {
        throw new JsonParseException("Recipe must be 3x3");
      }
      for (int j = 0; j < 3; ++j) {
        String c = row.substring(j, j + 1);
        if (" ".equals(c))
          continue;
        JsonElement value = key.get(c);
        if (value == null) {
          throw new JsonParseException("Key " + c + " not defined");
        }
        ingredients.set(3 * i + j, Ingredient.fromJson(value));
      }
    }
    return ingredients;
  }

  @Override
  public AssemblyTableRecipe read(Identifier identifier, JsonObject json) {
    AssemblyTableRecipeJsonFormat format =
        new Gson().fromJson(json, AssemblyTableRecipeJsonFormat.class);
    ItemStack stack = new ItemStack(
        Registry.ITEM.get(Identifier.tryParse(format.result.item)),
        Math.max(1, format.result.count));
    DefaultedList<Ingredient> ingredients =
        getIngredients(format.pattern, format.key);
    VakType vakType = ArcaneComputingRegistries.VAK_TYPE
        .get(Identifier.tryParse(format.vakType));
    if (vakType == null) {
      throw new JsonSyntaxException("Unknown vak type " + format.vakType + "!");
    }
    return new AssemblyTableRecipe(identifier, ingredients, vakType,
        format.vakAmount, stack);
  }

  @Override
  public AssemblyTableRecipe read(Identifier identifier,
      PacketByteBuf packetByteBuf) {
    DefaultedList<Ingredient> ingredients =
        DefaultedList.ofSize(9, Ingredient.EMPTY);
    for (int i = 0; i < 9; ++i) {
      ingredients.set(i, Ingredient.fromPacket(packetByteBuf));
    }
    int rawVakId = packetByteBuf.readInt();
    int vakAmount = packetByteBuf.readInt();
    ItemStack output = packetByteBuf.readItemStack();
    return new AssemblyTableRecipe(identifier, ingredients,
        ArcaneComputingRegistries.VAK_TYPE.get(rawVakId), vakAmount, output);
  }

  @Override
  public void write(PacketByteBuf packetByteBuf, AssemblyTableRecipe recipe) {
    DefaultedList<Ingredient> ingredients = recipe.getIngredients();
    for (int i = 0; i < 9; ++i) {
      ingredients.get(i).write(packetByteBuf);
    }
    packetByteBuf.writeInt(
        ArcaneComputingRegistries.VAK_TYPE.getRawId(recipe.getVakType()));
    packetByteBuf.writeInt(recipe.getVakAmount());
    packetByteBuf.writeItemStack(recipe.getOutput());

  }

}
