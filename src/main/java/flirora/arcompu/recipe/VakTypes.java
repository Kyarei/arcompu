package flirora.arcompu.recipe;

import flirora.arcompu.ArcaneComputingMod;
import flirora.arcompu.ArcaneComputingRegistries;
import flirora.arcompu.block.AssemblyTableBlockEntity;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.LightType;
import net.minecraft.world.World;

public class VakTypes {

  private static VakType register(String name, VakType type) {
    return Registry.register(ArcaneComputingRegistries.VAK_TYPE,
        new Identifier(ArcaneComputingMod.MOD_ID, name), type);
  }

  public static final VakType VAK_FIP = new VakType() {
    @Override
    public int amountGained(AssemblyTableBlockEntity table,
        int remainingNeeded) {
      World world = table.getWorld();
      int light = world.getLightLevel(LightType.SKY, table.getPos().up());
      if (light < 11)
        return 0;
      return 4 * (light - 11);
    }
  };

  public static final VakType VAK_LEP = new VakType() {
    @Override
    public int amountGained(AssemblyTableBlockEntity table,
        int remainingNeeded) {
      if (table.getAge() % 20 != 0)
        return 0;
      int amountGained = 0;
      World world = table.getWorld();
      for (int dx = -3; dx <= 3; ++dx) {
        for (int dz = -3; dz <= 3; ++dz) {
          BlockPos cur = table.getPos().add(dx, 0, dz);
          if (BlockTags.FLOWERS.contains(world.getBlockState(cur).getBlock())) {
            world.breakBlock(cur, false);
            amountGained += 10;
            if (amountGained <= remainingNeeded)
              break;
          }
        }
      }
      return amountGained;
    }
  };

  public static void registerAll() {
    register("vak_fip", VAK_FIP);
    register("vak_lep", VAK_LEP);
  }

}
