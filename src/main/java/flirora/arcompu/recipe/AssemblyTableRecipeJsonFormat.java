package flirora.arcompu.recipe;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class AssemblyTableRecipeJsonFormat {
  public class Output {
    public String item;
    public int count = 1;
  }

  public JsonArray pattern;
  public JsonObject key;
  public String vakType;
  public int vakAmount;
  public Output result;
}
