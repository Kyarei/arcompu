package flirora.arcompu.recipe;

import flirora.arcompu.ArcaneComputingRegistries;
import flirora.arcompu.block.AssemblyTableBlockEntity;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public abstract class VakType {

  public VakType() {
    //
  }

  public abstract int amountGained(AssemblyTableBlockEntity table,
      int remainingNeeded);

  public final TranslatableText getText() {
    Identifier id = ArcaneComputingRegistries.VAK_TYPE.getId(this);
    return new TranslatableText(
        "vak." + id.getNamespace() + "." + id.getPath());
  }

}
