package flirora.arcompu.recipe;

import flirora.arcompu.block.AssemblyTableBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.DefaultedList;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class AssemblyTableRecipe implements Recipe<AssemblyTableBlockEntity> {
  private final Identifier id;
  private final DefaultedList<Ingredient> ingredients;
  private final VakType vakType;
  private final int vakAmount;
  private final ItemStack output;

  public AssemblyTableRecipe(Identifier id,
      DefaultedList<Ingredient> ingredients, VakType vakType, int vakAmount,
      ItemStack output) {
    this.id = id;
    this.ingredients = ingredients;
    this.vakType = vakType;
    this.vakAmount = vakAmount;
    this.output = output;
  }

  @Override
  public boolean matches(AssemblyTableBlockEntity inventory, World world) {
    for (int i = 0; i < 9; ++i) {
      if (!ingredients.get(i).test(inventory.getInvStack(i)))
        return false;
    }
    return true;
  }

  @Override
  public ItemStack craft(AssemblyTableBlockEntity inventory) {
    return getOutput().copy();
  }

  @Override
  public boolean fits(int i, int j) {
    return true;
  }

  @Override
  public ItemStack getOutput() {
    return output;
  }

  @Override
  public Identifier getId() {
    return id;
  }

  public static class Type implements RecipeType<AssemblyTableRecipe> {
    private Type() {
    }

    public static final Type INSTANCE = new Type();

    public static final String ID = "assembly_table_recipe";
  }

  @Override
  public RecipeSerializer<?> getSerializer() {
    return AssemblyTableRecipeSerializer.INSTANCE;
  }

  @Override
  public RecipeType<?> getType() {
    return Type.INSTANCE;
  }

  public DefaultedList<Ingredient> getIngredients() {
    return ingredients;
  }

  public VakType getVakType() {
    return vakType;
  }

  public int getVakAmount() {
    return vakAmount;
  }

}
