package flirora.arcompu.block;

import flirora.arcompu.ArcaneComputingMod;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class SystemCoreBlock extends CasingBlock
    implements BlockEntityProvider {

  public static final Identifier ID =
      new Identifier(ArcaneComputingMod.MOD_ID, "system_core");

  public SystemCoreBlock(Settings settings) {
    super(settings);
  }

  @Override
  public BlockEntity createBlockEntity(BlockView blockView) {
    return new SystemCoreBlockEntity();
  }

  @Override
  public void onBlockRemoved(BlockState state, World world, BlockPos pos,
      BlockState newState, boolean moved) {
    if (state.getBlock() != newState.getBlock()) {
      BlockEntity blockEntity = world.getBlockEntity(pos);
      if (blockEntity instanceof Inventory) {
        ItemScatterer.spawn(world, pos, (Inventory) blockEntity);
        world.updateHorizontalAdjacent(pos, this);
      }

      super.onBlockRemoved(state, world, pos, newState, moved);
    }
  }

  @Override
  public ActionResult onUse(BlockState state, World world, BlockPos pos,
      PlayerEntity player, Hand hand, BlockHitResult hit) {
    if (world.isClient)
      return ActionResult.SUCCESS;

    BlockEntity be = world.getBlockEntity(pos);
    if (be != null && be instanceof SystemCoreBlockEntity) {
      ((SystemCoreBlockEntity) be).sync();
      ContainerProviderRegistry.INSTANCE.openContainer(SystemCoreBlock.ID,
          player, (packetByteBuf -> packetByteBuf.writeBlockPos(pos)));
    }

    return ActionResult.SUCCESS;
  }

}
