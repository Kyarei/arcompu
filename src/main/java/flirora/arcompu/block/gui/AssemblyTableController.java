package flirora.arcompu.block.gui;

import flirora.arcompu.block.AssemblyTableBlockEntity;
import flirora.arcompu.recipe.AssemblyTableRecipe;
import io.github.cottonmc.cotton.gui.CottonCraftingController;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.data.Alignment;
import net.minecraft.container.BlockContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.CraftingResultInventory;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;

public class AssemblyTableController extends CottonCraftingController {
  private CraftingResultInventory outputSlot;

  public AssemblyTableController(int syncId, PlayerInventory playerInventory,
      BlockContext context) {
    super(AssemblyTableRecipe.Type.INSTANCE, syncId, playerInventory,
        getBlockInventory(context), getBlockPropertyDelegate(context));

    this.outputSlot = new CraftingResultInventory() {

      @Override
      public boolean canPlayerUseInv(PlayerEntity playerEntity) {
        return false;
      }
    };

    WGridPanel rootPanel = (WGridPanel) getRootPanel();

    rootPanel
        .add(new WLabel(new TranslatableText("block.arcompu.assembly_table"),
            WLabel.DEFAULT_TEXT_COLOR), 0, 0);

    WItemSlot inputSlot = WItemSlot.of(blockInventory, 0, 3, 3);
    rootPanel.add(inputSlot, 2, 1);

    WItemSlot outputSlot =
        new WItemSlot(this.outputSlot, 0, 1, 1, true, false) {
          @Override
          public void tick() {
            AssemblyTableBlockEntity bi =
                (AssemblyTableBlockEntity) blockInventory;
            bi.updateInventory();
          }
        };
    rootPanel.add(outputSlot, 6, 2);

    WLabel vakDisplay = new WLabel("") {
      @Override
      public void tick() {
        AssemblyTableBlockEntity bi = (AssemblyTableBlockEntity) blockInventory;
        AssemblyTableRecipe currentRecipe = bi.getCurrentRecipe();
        if (currentRecipe != null) {
          this.setText(new TranslatableText("gui.arcompu.assembly_vak",
              bi.getVakAmount(), currentRecipe.getVakAmount(),
              currentRecipe.getVakType().getText()));
        } else {
          this.setText(new LiteralText(""));
        }
      }
    };
    rootPanel.add(vakDisplay, 4, 4);
    vakDisplay.setAlignment(Alignment.CENTER);
    vakDisplay.setLocation(vakDisplay.getX(), vakDisplay.getY() + 5);

    rootPanel.add(new WLabel(new TranslatableText("container.inventory"),
        WLabel.DEFAULT_TEXT_COLOR), 0, 5);

    rootPanel.add(this.createPlayerInventoryPanel(), 0, 6);

    rootPanel.validate(this);
  }

}
