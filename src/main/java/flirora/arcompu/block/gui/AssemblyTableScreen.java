package flirora.arcompu.block.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.entity.player.PlayerEntity;

@Environment(EnvType.CLIENT)
public class AssemblyTableScreen
    extends CottonInventoryScreen<AssemblyTableController> {

  public AssemblyTableScreen(AssemblyTableController container,
      PlayerEntity player) {
    super(container, player);
  }

}
