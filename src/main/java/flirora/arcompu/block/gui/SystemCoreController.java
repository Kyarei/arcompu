package flirora.arcompu.block.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.common.collect.Range;

import flirora.arcompu.ArcaneComputingMod;
import flirora.arcompu.block.SystemCoreBlockEntity;
import flirora.arcompu.item.io.AddressMap;
import flirora.arcompu.item.io.MappedPeripheralLocator;
import flirora.arcompu.item.io.PeripheralLocator;
import io.github.cottonmc.cotton.gui.CottonCraftingController;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WListPanel;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.container.BlockContext;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.PacketByteBuf;
import net.minecraft.util.Pair;

public class SystemCoreController extends CottonCraftingController {
  private final List<Pair<MappedPeripheralLocator, Boolean>> mappings =
      new ArrayList<>();

  private SystemCoreBlockEntity systemCore;

  public SystemCoreController(int syncId, PlayerInventory playerInventory,
      BlockContext context) {
    super(null, syncId, playerInventory, getBlockInventory(context),
        getBlockPropertyDelegate(context));

    this.systemCore = (SystemCoreBlockEntity) blockInventory;

    WGridPanel rootPanel = (WGridPanel) getRootPanel();

    rootPanel.add(new WLabel(new TranslatableText("block.arcompu.system_core"),
        WLabel.DEFAULT_TEXT_COLOR), 0, 0);

    WItemSlot cpuSlot = WItemSlot.of(blockInventory, 0);
    rootPanel.add(cpuSlot, 4, 1);

    WItemSlot peripheralSlots = WItemSlot.of(blockInventory, 1, 9, 1);
    rootPanel.add(peripheralSlots, 0, 3);

    rootPanel.add(new WLabel(new TranslatableText("container.inventory"),
        WLabel.DEFAULT_TEXT_COLOR), 0, 5);

    rootPanel.add(this.createPlayerInventoryPanel(), 0, 6);

    WListPanel<Pair<MappedPeripheralLocator, Boolean>, MappingWidget> mappingList =
        new WListPanel<Pair<MappedPeripheralLocator, Boolean>, MappingWidget>(
            mappings, () -> new MappingWidget(this, systemCore), (elem,
                widget) -> widget.setLocator(elem.getLeft(), elem.getRight())) {
          @Override
          public void tick() {
            super.tick();
            if (systemCore.shouldUpdateList()) {
              updateMappingPanel();
              this.layout();
              systemCore.onUpdatedList();
            }
          }
        };
    rootPanel.add(mappingList, 9, 0);
    mappingList.setSize(8 * 18, 9 * 18);

    rootPanel.validate(this);

    updateMappingPanel();
  }

  private void updateMappingPanel() {
    mappings.clear();
    AddressMap addressMap = systemCore.getAddressMap();
    addressMap.peripheralsAndMappings().forEach((e) -> {
      PeripheralLocator loc = e.getKey();
      Optional<Range<Integer>> orange = e.getValue();
      if (orange.isPresent()) {
        mappings.add(
            new Pair<>(new MappedPeripheralLocator(loc, orange.get()), false));
      } else {
        mappings
            .add(new Pair<>(new MappedPeripheralLocator(loc, -1, -1), true));
      }
    });
  }

  public void requestRebase(MappedPeripheralLocator locator, int newAddress) {
    if (!world.isClient()) {
      return;
    }
    PeripheralLocator main = locator.getLocator();
    // Send the packet
    PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
    data.writeBlockPos(systemCore.getPos());
    main.write(data);
    data.writeInt(newAddress);
    ClientSidePacketRegistry.INSTANCE
        .sendToServer(ArcaneComputingMod.REQUEST_REBASE_PACKET_ID, data);
  }

}
