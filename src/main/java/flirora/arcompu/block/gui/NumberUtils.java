package flirora.arcompu.block.gui;

public class NumberUtils {

  public static int parseAuto(String s) throws NumberFormatException {
    if (s.startsWith("0x")) {
      return Integer.parseInt(s.substring(2), 16);
    }
    if (s.startsWith("0b")) {
      return Integer.parseInt(s.substring(2), 2); // But why?
    }
    if (s.startsWith("0o")) {
      return Integer.parseInt(s.substring(2), 8);
    }
    return Integer.parseInt(s);
  }

}
