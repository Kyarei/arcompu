package flirora.arcompu.block.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

public class SystemCoreScreen
    extends CottonInventoryScreen<SystemCoreController> {

  public SystemCoreScreen(SystemCoreController container, PlayerEntity player) {
    super(container, player);
  }

}
