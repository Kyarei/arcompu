package flirora.arcompu.block.gui;

import flirora.arcompu.block.SystemCoreBlockEntity;
import flirora.arcompu.item.io.MappedPeripheralLocator;
import flirora.arcompu.item.io.Peripheral;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WTextField;
import net.minecraft.text.LiteralText;

public class MappingWidget extends WGridPanel {

  private SystemCoreController controller;
  private MappedPeripheralLocator locator;
  private boolean conflicts;
  private final WLabel name, end;
  private final WTextField start;
  private final SystemCoreBlockEntity core;

  public MappingWidget(SystemCoreController controller,
      SystemCoreBlockEntity core) {
    name = new WLabel("Peripheral");
    this.add(name, 0, 0);
    start = new WTextField() {
      @Override
      public void onFocusLost() {
        super.onFocusLost();
        updateLocator();
      }
    };
    this.add(start, 4, 0);
    end = new WLabel("");
    this.add(end, 6, 0);
    this.controller = controller;
    this.core = core;
  }

  public void setLocator(MappedPeripheralLocator locator, boolean conflicts) {
    this.locator = locator;
    this.conflicts = conflicts;
  }

  @Override
  public void tick() {
    super.tick();
    if (locator != null) {
      Peripheral p = locator.getLocator().getPeripheral(core.getWorld(), core);
      name.setText(p != null ? p.name() : new LiteralText("missingno"));
      if (conflicts) {
        if (!start.isFocused())
          start.setText("");
        end.setText(new LiteralText(""));
      } else {
        if (!start.isFocused())
          start.setText("" + locator.start());
        end.setText(new LiteralText("" + locator.end()));
      }
      if (conflicts)
        name.setColor(0xff_ff4d40, 0xff_ff4d40);
      else
        name.setColor(WLabel.DEFAULT_TEXT_COLOR,
            WLabel.DEFAULT_DARKMODE_TEXT_COLOR);
    }
  }

  private void updateLocator() {
    if (locator == null)
      return;
    String s = start.getText();
    if (s.isEmpty()) {
      controller.requestRebase(locator, -1);
    }
    try {
      int newAddress = NumberUtils.parseAuto(s);
      controller.requestRebase(locator, newAddress);
    } catch (NumberFormatException ex) {
      start.setText("" + locator.start());
    }
  }

}
