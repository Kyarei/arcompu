package flirora.arcompu.block;

import java.util.Optional;

import flirora.arcompu.ArcaneComputingMod;
import flirora.arcompu.recipe.AssemblyTableRecipe;
import flirora.arcompu.recipe.VakType;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.DefaultedList;
import net.minecraft.util.Tickable;

public class AssemblyTableBlockEntity extends BlockEntity
    implements ImplementedInventory, Tickable, BlockEntityClientSerializable {
  private DefaultedList<ItemStack> items =
      DefaultedList.ofSize(9, ItemStack.EMPTY);
  private AssemblyTableRecipe currentRecipe;
  private int vakAmount;
  private boolean dirty = false;
  private boolean first = true;
  long age = 0;

  public AssemblyTableBlockEntity() {
    super(ArcaneComputingMod.ASSEMBLY_TABLE_BLOCK_ENTITY);
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  @Override
  public void fromTag(CompoundTag tag) {
    super.fromTag(tag);
    Inventories.fromTag(tag, items);
    vakAmount = tag.getInt("VakAmount");
    age = tag.getLong("Age");
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    Inventories.toTag(tag, items);
    tag.putInt("VakAmount", vakAmount);
    tag.putLong("Age", age);
    return super.toTag(tag);
  }

  @Override
  public void fromClientTag(CompoundTag tag) {
    fromTag(tag);
  }

  @Override
  public CompoundTag toClientTag(CompoundTag tag) {
    return toTag(tag);
  }

  @Override
  public void tick() {
    if (first) {
      currentRecipe = calcCurrentRecipe().orElse(null);
      first = false;
    }
    updateInventory();
    if (currentRecipe != null) {
      if (!world.isClient) {
        int totalVakNeeded = currentRecipe.getVakAmount();
        VakType vakType = currentRecipe.getVakType();
        vakAmount += vakType.amountGained(this, totalVakNeeded - vakAmount);
        if (vakAmount >= totalVakNeeded) {
          craft();
        }
      }
    } else {
      vakAmount = 0;
    }
    if (!world.isClient) {
      this.sync();
    }
    ++age;
  }

  public void updateInventory() {
    if (!dirty)
      return;
    currentRecipe = calcCurrentRecipe().orElse(null);
    dirty = false;
  }

  private Optional<AssemblyTableRecipe> calcCurrentRecipe() {
    return world.getRecipeManager()
        .getFirstMatch(AssemblyTableRecipe.Type.INSTANCE, this, world);
  }

  private void craft() {
    vakAmount -= currentRecipe.getVakAmount();
    ItemStack output = currentRecipe.craft(this);
    ItemEntity entity = new ItemEntity(world, pos.getX() + 0.5,
        pos.getY() + 1.5, pos.getZ() + 0.5, output);
    world.spawnEntity(entity);
    for (ItemStack itemStack : items) {
      itemStack.decrement(1);
    }
    markDirty();
  }

  @Override
  public void markDirty() {
    super.markDirty();
    dirty = true;
  }

  public AssemblyTableRecipe getCurrentRecipe() {
    return currentRecipe;
  }

  public int getVakAmount() {
    return vakAmount;
  }

  public long getAge() {
    return age;
  }

}
