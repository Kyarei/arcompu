package flirora.arcompu.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.Direction;

public class CasingBlock extends Block {

  public static final DirectionProperty HORIZONTAL_FACING =
      Properties.HORIZONTAL_FACING;

  public CasingBlock(Settings settings) {
    super(settings);
    setDefaultState(stateManager.getDefaultState().with(HORIZONTAL_FACING,
        Direction.NORTH));
  }

  @Override
  protected void appendProperties(Builder<Block, BlockState> builder) {
    super.appendProperties(builder);
    builder.add(HORIZONTAL_FACING);
  }

  @Override
  public BlockState getPlacementState(
      ItemPlacementContext itemPlacementContext) {
    return getDefaultState().with(HORIZONTAL_FACING,
        itemPlacementContext.getPlayerFacing().getOpposite());
  }

}
