package flirora.arcompu.block;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Streams;
import com.mojang.datafixers.util.Pair;

import blue.endless.jankson.annotation.Nullable;
import flirora.arcompu.ArcaneComputingMod;
import flirora.arcompu.item.cpu.CpuItem;
import flirora.arcompu.item.cpu.CpuState;
import flirora.arcompu.item.io.AddressMap;
import flirora.arcompu.item.io.MappedPeripheralLocator;
import flirora.arcompu.item.io.Peripheral;
import flirora.arcompu.item.io.PeripheralItem;
import flirora.arcompu.item.io.PeripheralLocator;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.DefaultedList;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class SystemCoreBlockEntity extends BlockEntity implements
    ImplementedInventory, Peripheral, Tickable, BlockEntityClientSerializable {
  public static final int MAX_PERIPHERALS = 9;
  // 0: cpu, 1 -- 9: item peripherals
  private DefaultedList<ItemStack> items =
      DefaultedList.ofSize(1 + MAX_PERIPHERALS, ItemStack.EMPTY);
  private AddressMap addressMap;
  private CpuState<?> cpuState;
  private int cyclesOwed;
  private int age;
  private boolean shouldUpdateList;

  public SystemCoreBlockEntity() {
    super(ArcaneComputingMod.SYSTEM_CORE_BLOCK_ENTITY);
    this.addressMap = new AddressMap();
  }

  @Override
  public void fromTag(CompoundTag tag) {
    super.fromTag(tag);
    Inventories.fromTag(tag, items);
    cpuState = null;
    CpuItem item = getCpu();
    if (item != null) {
      cpuState = item.getBootState();
      CompoundTag stateTag = tag.getCompound("CpuState");
      if (stateTag != null) {
        cpuState.fromTag(stateTag);
      }
    }
    cyclesOwed = tag.getInt("CyclesOwed");
    age = tag.getInt("Age");
    addressMap.readFromTag(tag);
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    Inventories.toTag(tag, items);
    if (cpuState != null) {
      CompoundTag stateTag = new CompoundTag();
      cpuState.toTag(stateTag);
      tag.put("CpuState", stateTag);
    }
    tag.putInt("CyclesOwed", cyclesOwed);
    tag.putInt("Age", age);
    addressMap.writeToTag(tag);
    return super.toTag(tag);
  }

  @Override
  public void fromClientTag(CompoundTag tag) {
    fromTag(tag);
    shouldUpdateList = true;
  }

  @Override
  public CompoundTag toClientTag(CompoundTag tag) {
    return toTag(tag);
  }

  @Override
  public int getAddressSpace() {
    CpuItem cpu = getCpu();
    return cpu != null ? cpu.getAddressSpaceSize() : 0;
  }

  @Nullable
  private Pair<Peripheral, Integer> addressToPeripheralMemoryLocation(
      int address) {
    Pair<PeripheralLocator, Integer> deref = addressMap.dereference(address);
    if (deref == null)
      return null;
    return Pair.of(deref.getFirst().getPeripheral(world, this),
        deref.getSecond());
  }

  private int findPeripheralOffset(int size) {
    CpuItem cpu = getCpu();
    if (cpu == null)
      return -1;
    return addressMap.findFreeOffset(size, cpu.getAddressSpaceSize());
  }

  @Override
  public byte peek(int offset) {
    Pair<Peripheral, Integer> location =
        addressToPeripheralMemoryLocation(offset);
    if (location == null || location.getFirst() == null)
      return 0;
    return location.getFirst().peek(location.getSecond());
  }

  @Override
  public boolean poke(int offset, byte b) {
    Pair<Peripheral, Integer> location =
        addressToPeripheralMemoryLocation(offset);
    if (location == null || location.getFirst() == null)
      return false;
    return location.getFirst().poke(location.getSecond(), b);
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  private CpuItem getCpu() {
    ItemStack itemStack = items.get(0);
    Item item = itemStack.getItem();
    if (item instanceof CpuItem)
      return (CpuItem) item;
    return null;
  }

  public PeripheralItem getPeripheralItem(int i) {
    ItemStack itemStack = items.get(1 + i);
    Item item = itemStack.getItem();
    if (item instanceof PeripheralItem)
      return (PeripheralItem) item;
    return null;
  }

  public Peripheral getPeripheral(int i) {
    PeripheralItem item = getPeripheralItem(i);
    if (item == null)
      return null;
    return item.toPeripheral(getInvStack(1 + i));
  }

  public boolean isRunning() {
    return cpuState != null;
  }

  @Override
  public void tick() {
    if (isRunning()) {
      CpuItem cpu = getCpu();
      if (cpu == null) {
        cpuState = null;
        return;
      }
      int allotted = cpu.getClockSpeed() - cyclesOwed;
      int used = cpuState.advance(this, allotted);
      cyclesOwed = used > allotted ? used - allotted : 0;
    }
    if (age % 20 == 0) {
      updateBlockPeripheralInfo();
    }
    ++age;
  }

  @Override
  public void setInvStack(int slot, ItemStack stack) {
    if (slot == 0) {
      cpuState = null;
    } else {
      if (stack.getItem() instanceof PeripheralItem) {
        Peripheral p = ((PeripheralItem) stack.getItem()).toPeripheral(stack);
        int size = p.getAddressSpace();
        int start = findPeripheralOffset(size);
        addressMap.add(new MappedPeripheralLocator(
            PeripheralLocator.fromItemIndex(slot - 1), start, start + size));
      } else {
        PeripheralLocator loc = PeripheralLocator.fromItemIndex(slot - 1);
        addressMap.remove(loc);
      }
    }
    ImplementedInventory.super.setInvStack(slot, stack);
    if (!world.isClient)
      sync();
  }

  public void boot() {
    CpuItem cpu = getCpu();
    if (cpu != null) {
      cpuState = cpu.getBootState();
    }
  }

  private List<Pair<Vec3i, Peripheral>> seekBlockPeripherals() {
    Set<BlockPos> visited = new HashSet<>();
    Queue<BlockPos> pending = new ArrayDeque<>();
    List<Pair<Vec3i, Peripheral>> discovered = new ArrayList<>();
    pending.add(pos.up());
    pending.add(pos.down());
    pending.add(pos.north());
    pending.add(pos.south());
    pending.add(pos.east());
    pending.add(pos.west());
    while (true) {
      BlockPos curr = pending.poll();
      if (curr == null)
        break;
      visited.add(curr);
      BlockState state = world.getBlockState(curr);
      BlockEntity entity = world.getBlockEntity(curr);
      // Prevent funni shit from happening when you try to connect two system
      // cores together.
      if (entity != null && entity instanceof Peripheral
          && !(entity instanceof SystemCoreBlockEntity)) {
        discovered.add(Pair.of(curr.subtract(pos), (Peripheral) entity));
      }
      if (state.getBlock() == ArcaneComputingMod.CONDUCTIVE_FABRIC) {
        for (BlockPos next : new BlockPos[] { curr.up(), curr.down(),
            curr.north(), curr.south(), curr.east(), curr.west() }) {
          if (!visited.contains(next))
            pending.add(next);
        }
      }
    }
    return discovered;
  }

  private void updateBlockPeripheralInfo() {
    List<Pair<Vec3i, Peripheral>> peripherals = seekBlockPeripherals();
    Map<Vec3i, Peripheral> knownOffsets = peripherals.stream()
        .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));
    // Remove missing or suspicious peripheral entries
    addressMap.removeIf((elem) -> {
      Vec3i off = elem.getLocator().getBlockOffset();
      if (off != null) {
        Peripheral p = knownOffsets.get(off);
        if (p == null || p.getAddressSpace() != elem.end() - elem.start())
          return true;
      }
      return false;
    });
    // Add new entries
    Set<Vec3i> alreadyRegistered = addressMap.mappings()
        .flatMap((elem) -> Streams
            .stream(Optional.ofNullable(elem.getLocator().getBlockOffset())))
        .collect(Collectors.toSet());
    for (Pair<Vec3i, Peripheral> entry : peripherals) {
      Vec3i offset = entry.getFirst();
      Peripheral peripheral = entry.getSecond();
      if (alreadyRegistered.contains(offset))
        continue;
      int size = peripheral.getAddressSpace();
      int start = findPeripheralOffset(size);
      addressMap.add(new MappedPeripheralLocator(
          PeripheralLocator.fromBlockOffset(offset), start, start + size));
    }
  }

  @Override
  public boolean isValidInvStack(int slot, ItemStack itemStack) {
    if (slot == 0)
      return itemStack.getItem() instanceof CpuItem;
    else
      return itemStack.getItem() instanceof PeripheralItem;
  }

  public AddressMap getAddressMap() {
    return addressMap;
  }

  @Override
  public Text name() {
    return new TranslatableText("block.arcompu.system_core");
  }

  public boolean tryRebase(PeripheralLocator locator, int newAddress) {
    CpuItem cpu = getCpu();
    if (cpu == null)
      return false;
    Peripheral p = locator.getPeripheral(world, this);
    if (p == null)
      return false;
    boolean stat = newAddress == -1 ? addressMap.unbase(locator)
        : addressMap.tryRebase(locator, newAddress, p.getAddressSpace(),
            cpu.getAddressSpaceSize());
    if (stat)
      sync();
    return stat;
  }

  public boolean shouldUpdateList() {
    return shouldUpdateList;
  }

  public void onUpdatedList() {
    shouldUpdateList = false;
  }

}
