# arcompu

Arcane Computing – computing for the refined.

## What is it?

Magic-themed computer mod where you don't just go waltzing in jamming Lua code.

## TODO

* Actually add the real shit

## License

MIT (see LICENSE).

This mod uses code from [this implementation of an interval tree](https://github.com/charcuterie/interval-tree),
which is also under an MIT license.
